/*
 * Gabe Kelly
 * Dr. Kapfhammer
 * CS 440
 * February 7th, 2014
 * This is the file where all of the computational
 * analysis and like wise go
 */
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;

public class Operations
{
    private float max;

    private float min;

    private float average;

    private float total;

    public float computeMax(HashMap<String, Float> map)
    {
        Iterator<Float> temp = map.values().iterator();
        ArrayList<Float> data = new ArrayList<Float>();

        while(temp.hasNext())
        {
            float value = temp.next();
            data.add(value);
        }
            
        max = data.get(0);

        for(int i = 1; i < data.size(); i++)
        {
            if (data.get(i) > max)
                max = data.get(i);
        }

        return max;

    }

    public float computeMin(HashMap<String, Float> map)
        {
            Iterator<Float> temp = map.values().iterator();
            ArrayList<Float> data = new ArrayList<Float>();

            while(temp.hasNext())
            {
                float value = temp.next();
                data.add(value);
            }

            min = data.get(0);

            for(int i = 1; i < data.size(); i++)
            {
                if (data.get(i) < min)
                    min = data.get(i);
            }

            return min;
        }

    public float computeAvg(HashMap<String, Float> map)
        {
            Iterator<Float> temp = map.values().iterator();
            ArrayList<Float> data = new ArrayList<Float>();

            while(temp.hasNext())
            {
                float value = temp.next();
                data.add(value);
            }

            for(int i = 0; i < data.size(); i++)
            {
                average += data.get(i);
            }

            average = (float) average/data.size();
            
            return average;
        }

    public float computeMedian(HashMap<String, Float> map)
    {
        Iterator<Float> temp = map.values().iterator();
        ArrayList<Float> data = new ArrayList<Float>();

        while(temp.hasNext())
        {
            float value = temp.next();
            data.add(value);
        }

        QuickSort sort = new QuickSort();

        data = sort.QuickSort(data);
        
        int middle = (int) data.size()/2;

        return data.get(middle);
    }
    
    public float computeTotal(HashMap<String, Float> map)
        {
        Iterator<Float> temp = map.values().iterator();
        ArrayList<Float> data = new ArrayList<Float>();

        while(temp.hasNext())
        {
            float value = temp.next();
            data.add(value);
        }

            for(int i = 0; i < data.size(); i++)
            {
                total += data.get(i);
            }

            return total;
        }
}
