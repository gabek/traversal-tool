/*
 * Gabe Kelly
 * Dr. Kapfhammer
 * CS 440
 * February 9th, 2014
 * This is a program to write out a CSV file
 * containing the data that I have collected
 */
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Iterator;

public class CSVWriter
{
    public void writeCSV(HashMap<String, Float> data, String name)  
    {
        try{
        FileWriter writer = new FileWriter(name); 
        writer.append("FileName,FileSize");
        writer.append('\n');

        Iterator<Entry<String, Float>> itr = data.entrySet().iterator();

        while(itr.hasNext())
        {
            Entry<String, Float> entry = itr.next();
            writer.append(entry.getKey() + "," + entry.getValue());
            writer.append('\n');
        }

        
        writer.flush();
        writer.close();
        }
        catch(IOException e)
        {
        }
    }
}

