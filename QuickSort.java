/*
 * Gabe Kelly
 * Professor Kapfhammer
 * CS 440
 * February 9th, 2014
 * This is a method so that I can
 * implement my median calculation 
 * in operations
 */
import java.util.ArrayList;

public class QuickSort
{
    public ArrayList<Float> QuickSort (ArrayList<Float> input)
    {
        int middle = (int) input.size()/2;
        float pivot = input.get(middle);
        input.remove(middle);

        ArrayList<Float> equivalent = new ArrayList<Float>();//in case multiple files equals the pivot
        ArrayList<Float> smaller = new ArrayList<Float>();//file sizes less than pivot
        ArrayList<Float> greater = new ArrayList<Float>();//file sizes greater than pivot

        for(int i = 0; i < input.size(); i++)
        {
            if(input.get(i) <= pivot)
            {
                if(input.get(i) != pivot)
                {
                    smaller.add(input.get(i));
                }
                else
                {
                    equivalent.add(input.get(i));

                }
            }
            else
            {
                greater.add(input.get(i));
            }
        }
        if(smaller.size() > 1)
            smaller = QuickSort(smaller);
        
        if(greater.size() > 1)
            greater = QuickSort(greater);

        
        ArrayList<Float> sorted = new ArrayList<Float>();

        for(int i = 0; i < smaller.size(); i++)
        {
            sorted.add(smaller.get(i));
        }

        sorted.add(pivot);
        
        for(int i = 0; i <equivalent.size(); i++)
        {
            sorted.add(equivalent.get(i));
        }

        for(int i = 0; i < greater.size(); i++)
        {
            sorted.add(greater.get(i));
        }

        return sorted;
    }

    /*public static void main(String[] args)
    {
        traversalTool tool = new traversalTool();
        
        String path = "/home/k/kellyg/cs111/lab1";

        ArrayList<Float> sort = tool.traverse(path);

        for(int i = 0; i < sort.size() - 1; i++)
        {
            System.out.print(sort.get(i) + " ");
        }
        System.out.println(sort.get(sort.size()- 1));

        QuickSort quick = new QuickSort();

        sort = quick.QuickSort(sort);

        /*for(int i = 0; i < sort.size() - 1; i++)
        {
            System.out.print(sort.get(i) + " ");
        }
        System.out.println(sort.get(sort.size()- 1));*/
}
