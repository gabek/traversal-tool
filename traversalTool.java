/*
 * Gabe Kelly
 * Dr. Kapfhammer
 * CS 440
 * February 3rd, 2014
 * A tool to traverse the file system
 * starting with a specified file root
 */
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.util.HashMap;


public class traversalTool
{

    public HashMap<String, Float> traverse(String path)
        {
            File root = new File(path);

            HashMap<String, Float> dataSize = new HashMap<String, Float>();

            File[] list = root.listFiles();

            for (int i = 0; i < list.length; i++)
            {
                if (list[i].isDirectory())
                {
                    dataSize = subTraverse(list[i].getPath(), dataSize);
                }
                else
                {
                    dataSize.put(list[i].getName(), (float) list[i].length());
                }
                
            }

            return dataSize;
        }

    public HashMap<String, Float> subTraverse(String path, HashMap<String, Float> dataSize)
    {
        File root = new File(path);

        File[] list = root.listFiles();

            for (int i = 0; i < list.length; i++)
            {
                dataSize.put(list[i].getName(), (float) list[i].length());

                if (list[i].isDirectory())
                {
                    dataSize = subTraverse(list[i].getPath(), dataSize);
                }
                
            }

            return dataSize;
    }

}



