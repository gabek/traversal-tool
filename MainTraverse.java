/*
 * Gabe Kelly
 * Dr Kapfhammer
 * CS 440
 * February 8th, 2014
 * This is the main class of the 
 * java traversal tool that I have made
 * for the second lab of cs440.
 */
import java.util.ArrayList;
import java.io.File;
import java.text.ParseException;
import java.io.IOException;
import java.util.Scanner;
import java.util.HashMap;
//import com.beust.jcommander.*;
//import com.beust.jcommander.ParameterException;

public class MainTraverse
{
    public static void main(String[] args)
    {       
        Operations operations = new Operations();

        traversalTool tool = new traversalTool();

        ArrayList<Float> result = new ArrayList<Float>();
        
       
        HashMap<String, Float> data = tool.traverse(args[0]);
            switch(args[1])
                    {
                        case "max": result.add(operations.computeMax(data));
                                    System.out.println("The Largest file has a byte size of " + result.get(0));
                                    break;

                        case "min": result.add(operations.computeMin(data));
                                    System.out.println("The Smallest file has a byte size of " + result.get(0));
                                    break;

                        case "avg": result.add(operations.computeAvg(data));
                                    System.out.println("The average file has a byte size of " + result.get(0));
                                    break;

                        case "med": result.add(operations.computeMedian(data));
                                    System.out.println("The median of the files has a byte size of " + result.get(0));
                                    break;

                        case "total": result.add(operations.computeTotal(data));
                                      System.out.println("The total byte size of the file system is " + result.get(0));
                                      break;

                        case "all": result.add(operations.computeMax(data));
                                    result.add(operations.computeMin(data));
                                    result.add(operations.computeAvg(data));
                                    result.add(operations.computeMedian(data));
                                    result.add(operations.computeTotal(data));
                                    System.out.println("The Largest file has a byte size of " + result.get(0));
                                    System.out.println("The Smallest file has a byte size of " + result.get(1));
                                    System.out.println("The average file has a byte size of " + result.get(2));
                                    System.out.println("The median of the files has a byte size of " + result.get(3));
                                    System.out.println("The total byte size of file system is" + result.get(4));
                                    break;


                    }
            if(args[2].equals("file"))
            {   
                System.out.println("Enter the name of the file you want to make:");
                System.out.println("Remember to have it end in .csv");
                Scanner scan = new Scanner(System.in);
                String file = scan.nextLine();
                CSVWriter output = new CSVWriter();
                output.writeCSV(data, file);
            }
        
    }
}



